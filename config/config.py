#!/usr/bin/env python

scriptTitle = "Bookmarks2link"
year	    = "2011-2012"
gitlink     = "https://bitbucket.org/pyro46/bookmarks2link"
gitbug     = "https://bitbucket.org/pyro46/bookmarks2link/issues"
appengineimage = "http://code.google.com/appengine/images/appengine-silver-120x30.gif"
appenginealt = "Google App Engine"
