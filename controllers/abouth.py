#!/usr/bin/env python

import os
from datetime import datetime
from config import config

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

class About(webapp.RequestHandler):
	def get(self):
		# We are using the template module to output the page.
		path = os.path.join(os.path.dirname(__file__), '../views' ,'about.html')
	
		values = {
			'year': config.year,
			'title': config.scriptTitle,
			'gitlink': config.gitlink,
			'appengineimage': config.appengineimage,
			'appenginealt': config.appenginealt,
		}

		self.response.out.write(template.render(path, values))
