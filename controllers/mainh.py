#!/usr/bin/env python

import os
import sys
from datetime import datetime
from config import config

# Including the models:
from models.models import *

from google.appengine.api import users
from google.appengine.ext import db

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

# This controller handles the
# generation of the front page.
PAGE_SIZE = 5

class MainPage(webapp.RequestHandler):
	def get(self,page=1):
		# We are using the template module to output the page.
		path = os.path.join(os.path.dirname(__file__), '../views' ,'index.html')
	
		user = users.get_current_user()
		url = users.create_login_url(self.request.uri)
		url_linktext = 'Login'
					
		if user:
			url = users.create_logout_url(self.request.uri)
			url_linktext = 'Logout'

		# GQL is similar to SQL    
		lnkquery = LnkmeModel.gql("WHERE author = :author ", author=users.get_current_user())
		
		
		#Paging Logic goes here 
		page = self.request.get('page') or page
		page = int(page) - 1

		lnks = lnkquery.fetch(PAGE_SIZE, page * PAGE_SIZE)
		
		lnkcount = lnkquery.count()
		delta = 1 if lnkcount % PAGE_SIZE == 0 else 2 
		range_page = range(1, (lnkcount / PAGE_SIZE) + delta )
		values = {
			'year': config.year,
			'title': config.scriptTitle,
			'gitlink': config.gitlink,
			'gitbug': config.gitbug,
			'appengineimage': config.appengineimage,
			'appenginealt': config.appenginealt,
			'lnks': lnks,
			'numberlnks' : lnkquery.count(),
			'user': user,
			'url': url,
			'url_linktext': url_linktext,
			'range_page': range_page,
		}

		self.response.out.write(template.render(path, values))

#create new link in lnk
class AddNew(webapp.RequestHandler):
	def post(self):
		user = users.get_current_user()
		bookmarkurl = self.request.get('url')
		description = self.request.get('description')
		if bookmarkurl :
			if not bookmarkurl.startswith("http://"):
				bookmarkurl = "http://" + bookmarkurl 
			if description :
				lnk = LnkmeModel(author = user, description = description, url = bookmarkurl, finished = False)
				lnk.put()
				self.redirect('/')
			else:
				self.redirect('/')
		else:
			self.redirect('/')	        

# This class deletes the selected bookmarks
class DeleteLink(webapp.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if user:
			id = int(self.request.get('id'))
			lnk = LnkmeModel.get_by_id(id)
	    		if lnk:
		            	lnk.delete()
				self.redirect('/') 
		else:
			self.redirect('/')


class UpdateLink(webapp.RequestHandler):
	def get(self):
		user = users.get_current_user()
		if self.request.get('id') != '' and user:
			lnkid = int(self.request.get('id'))
			text = self.request.get('text')
			subtext = self.request.get('subtext')
			#print >> sys.stderr, (subtext,text,lnkid)
			lnkquery = LnkmeModel.get_by_id(lnkid)
			if lnkquery:
				lnkquery.description = text
				lnkquery.url = subtext	
				lnkquery.put()	
			else:
				self.redirect('/')
		else:
			self.redirect('/')

