#!/usr/bin/env python

# Importing the controllers that will handle
# the generation of the pages:
from controllers import mainh, errorh, abouth

# Importing some of Google's AppEngine modules:
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util


def main():
	# Register the URL with the responsible classes
	application = webapp.WSGIApplication([
		('/', mainh.MainPage),
		('/index',mainh.MainPage),
		('/addnew', mainh.AddNew),
		('/delete', mainh.DeleteLink),
		('/save', mainh.UpdateLink),
		('/404', errorh.NotFound),
		('/about', abouth.About),
	#	('/email', Email),
	#	('/sendmail',sendmail)
	],debug=True)
	
	# Register the wsgi application to run
	util.run_wsgi_app(application)


if __name__ == '__main__':
	main()
