#!/usr/bin/env python

from google.appengine.ext import db

# These classes define the data objects
# that you will be able to store in
# AppEngine's data store.

class LnkmeModel(db.Model):
	author		= db.UserProperty(required=True)
	description	= db.StringProperty(required=True)
	url 	 	= db.StringProperty()
	created         = db.DateTimeProperty(auto_now_add=True)
	updated 	= db.DateTimeProperty(auto_now=True)
	finished        = db.BooleanProperty()
