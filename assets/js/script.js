$(document).ready(function(){
	/* The following code is executed once the DOM is loaded */
	var currentTODO;
	// Listening for a click on a edit button
	
	$('.bookmarklist a.edit').live('click',function(e){
		
		currentTODO = $(this).closest('.lnklist');
		currentTODO.data('id',currentTODO.attr('id').replace('todo-',''));
		e.preventDefault();
	});


	$('.bookmarklist a.edit').live('click',function(){

		//$('.bookmarklist').replaceWith(previousState);
		var container = currentTODO.find('.text');
		var subcontainer = currentTODO.find('.subtext');

		if(!currentTODO.data('origText')){
			// Saving the current value of the bookmark so we can
			// restore it later if the user discards the changes:
			currentTODO.data('origText',container.text());
		} else {
			// This will block the edit button if the edit box is already open:
			return false;
		}

		if(!currentTODO.data('origsubtext')){
			currentTODO.data('origsubText',subcontainer.text());
		} else { 
			return false;
		}
		
		$('<input class="inputtext">').val(container.text()).appendTo(container.empty());
		$('<input class="inputsubtext">').val(subcontainer.text()).appendTo(subcontainer.empty());

		// Appending the save and cancel links:
		currentTODO.append(
			'<div class="editTodo">'+
				'<a class="saveChanges" href="#">Save</a> or <a class="discardChanges" href="#">Cancel</a>'+
			'</div>'
		);
	});
	
	// The cancel edit link:
	$('.bookmarklist a.discardChanges').live('click',function(){
		currentTODO.find('.text').text(currentTODO.data('origText')).end().removeData('origText');
		var link = $('<a target="_blank"/>').text(currentTODO.data('origsubText'));
		link.attr('href',currentTODO.data('origsubText'));
		currentTODO.find('.subtext').html(link);
		currentTODO.find('.editTodo').remove();
	});
	
	$('.bookmarklist a.saveChanges').live('click',function(e){
		var text = currentTODO.find(".inputtext").val();
		var subtext = currentTODO.find(".inputsubtext").val();
		$.get("/save",	
			{'id':currentTODO.data('id'),'text':text,'subtext':subtext}
			);
		currentTODO.removeData('origText').find(".text").text(text);
		var link = $('<a target="_blank"/>').text(subtext);
		link.attr('href',subtext);
		currentTODO.removeData('origsubText').find(".subtext").html(link);
		currentTODO.find('.editTodo').remove();
		e.preventDefault();
	});

}); // Closing $(document).ready()

function toggleSlide(objname){
	if(document.getElementById(objname).style.display == "none"){
		slidedown(objname);
	} else {
		slideup(objname);
	}
}

function confirm_delete(){
	if (confirm('Are you sure want to delete bookmark ?')) {
		return true;
	} else {
		return false;
	}
}

function ValidateForm() {
	var description = document.getElementById('description');
	var url = document.getElementById('url');

	if (description.value == "") {
		description.style.background = "red";
		alert("Description can not be empty !!!");
		return false;
	} else if (url.value == "") {
		url.style.background = "red";
		alert("Link for description can not be empty !!!");
		return false;
	} else {
		description.style.background = "white"; 
		url.style.background = "white"; 
		return true;
	}
}

